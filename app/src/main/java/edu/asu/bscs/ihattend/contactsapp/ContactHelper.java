/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version March 2, 2015
 */

package edu.asu.bscs.ihattend.contactsapp;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

public class ContactHelper {

	private final Context context;

	public ContactHelper(Context context) {
		this.context = context;
	}

	public List<String> getContactNames() {
		List<String> names = new ArrayList<>();
		String whereName = ContactsContract.Data.MIMETYPE + " = ?";
		String[] whereNameParams = new String[] {
				ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE
		};


		Cursor nameCursor = context.getContentResolver().query(
				ContactsContract.Data.CONTENT_URI,
				null,
				whereName,
				whereNameParams,
				ContactsContract.CommonDataKinds.StructuredName.GIVEN_NAME
		);

		while (nameCursor.moveToNext()) {
			int columnIndex = nameCursor.getColumnIndex(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME);
			names.add(nameCursor.getString(columnIndex));
		}
		nameCursor.close();

		return names;
	}

	public String getPhone(String name) {
		String phone = "noPhone";

		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " = ?",
				new String[] { name }, null);
		if (cursor.moveToFirst()) {
			int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
			String contactId = cursor.getString(idIndex);
			Cursor phoneCursor = resolver.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { contactId }, null);
			if (phoneCursor.moveToNext()) {
				int numberIndex = phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);
				phone = phoneCursor.getString(numberIndex);
			}
			phoneCursor.close();
		}
		cursor.close();
		return phone;
	}

	public String getEmail(String name) {
		String email = "noEmail";

		ContentResolver resolver = context.getContentResolver();
		Cursor cursor = resolver.query(ContactsContract.Contacts.CONTENT_URI, null, ContactsContract.Contacts.DISPLAY_NAME_PRIMARY + " = ?",
				new String[] { name }, null);
		if (cursor.moveToFirst()) {
			int idIndex = cursor.getColumnIndex(ContactsContract.Contacts._ID);
			String contactId = cursor.getString(idIndex);
			Cursor emailCursor = resolver.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
					ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?", new String[] { contactId }, null);
			if (emailCursor.moveToNext()) {
				int numberIndex = emailCursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.ADDRESS);
				email = emailCursor.getString(numberIndex);
			}
			emailCursor.close();
		}
		cursor.close();
		return email;
	}
}
