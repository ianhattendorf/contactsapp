/*
 *		The MIT License (MIT)
 *
 *
 *		Copyright (c) 2015 Ian Hattendorf
 *
 * 		Permission is hereby granted, free of charge, to any person obtaining a copy
 *		of this software and associated documentation files (the "Software"), to deal
 *		in the Software without restriction, including without limitation the rights
 *		to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 *		copies of the Software, and to permit persons to whom the Software is
 *		furnished to do so, subject to the following conditions:
 *
 *		The above copyright notice and this permission notice shall be included in
 *		all copies or substantial portions of the Software.
 *
 *		THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 *		IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 *		FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 *		AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 *		LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 *		OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 *		THE SOFTWARE.
 *
 *		@author Ian Hattendorf mailto:Ian.Hattendorf@asu.edu
 *
 * 		@version March 2, 2015
 */

package edu.asu.bscs.ihattend.contactsapp;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;

import java.util.ArrayList;
import java.util.List;


public class MainActivity extends ActionBarActivity {

	private static final String Tag = MainActivity.class.getSimpleName();

	private ArrayList<String> names;
	private ArrayAdapter<String> adapter;

	private ContactHelper contactHelper;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		contactHelper = new ContactHelper(this);
		List<String> contactNames = contactHelper.getContactNames();

		names = new ArrayList<>(contactNames);
		adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, contactNames);
		final ListView listView = (ListView) findViewById(R.id.waypointListView);
		listView.setAdapter(adapter);
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
				Log.d(Tag, String.format("position: %s, id: %s", position, id));
				String name = names.get(position);
				Log.d(Tag, "clicked name: " + name);
				String phone = contactHelper.getPhone(name);
				String email = contactHelper.getEmail(name);

				Intent intent = new Intent(view.getContext(), ViewContact.class);
				intent.putExtra("name", name);
				intent.putExtra("email", email);
				intent.putExtra("phone", phone);
				startActivity(intent);
			}
		});

		refreshContacts(null);
	}


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	public void refreshContacts(View view) {
		Log.d(Tag, "refreshContacts()");
		names.clear();
		List<String> newNames = contactHelper.getContactNames();
		Log.d(Tag, "new names:");
		for (String name : newNames)
			Log.d(Tag, "name: " + name);
		names.addAll(newNames);
		adapter.notifyDataSetChanged();
		Log.d(Tag, "adapter notified of changes");
	}

	public void addContact(View view) {
		startActivity(new Intent(this, AddContact.class));
	}
}
